DROP KEYSPACE bxfxdb;

CREATE KEYSPACE bxfxdb
	WITH strategy_class = 'SimpleStrategy'
	AND strategy_options:replication_factor='1';

USE bxfxdb;

CREATE TABLE ccycachestr (
	type varchar,
	lastupdate varchar,
	xmldata varchar,
	PRIMARY KEY (type)
);

CREATE TABLE ccycache (
	id varchar,
	type varchar,
	lastupdate timestamp,
	xmldata blob,
	PRIMARY KEY (id)
);



