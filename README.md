# Engineering Challenge -- Currency rates

The code in this repository is created for an engineering challenge. The goal was to create a non-blocking web application for retrieving currency codes and graphically displaying the 90-day performance of a selected currency against the EURO.

The application is implemented using the Play 1.2.5 framework. 

## Update

As a second phase to this challenge, the system was updated to incorporate the use of a Cassandra data-store to be used as a cache for the results of 3rd-party API requests. The code makes use of Netflix's Astyanax client libraries. 



## Server side

On the server side, we have:

* A simple html page delivering the view
* Controller code for responding to AJAX requests from the client, delivering results in JSON format
* Some simple models for holding data (primarily to be used with GSON)

One of the biggest requirements on the server side was or the application to be _non-blocking_. The idea here is that the server should not get held up waiting for long running computations or operations that are generated as the result of a client request. In the play framework, this is achieved using a **Promise**. In this application, the long-running operation is the fetching of a set of currency data from an external website, and is achieved in the code as follows:

	private static Document getResultXml(String url) {
		F.Promise<WS.HttpResponse> r1 = WS.url(url).getAsync();
		WS.HttpResponse httpResponse = await(r1);
		Document xmlDoc = httpResponse.getXml();
		return xmlDoc;
	}

With the data fetched in XML format, the next task is to filter it to extract only the subset that is requested by the client. This is achieved through the use of XPath selections, stripping the results from the XML, populating simple POJOs and sending them back to the client as JSON (using the GSON library).

## Client side
The client side uses twitter bootstrap stylesheets, jQuery for Ajax requests and data manipulation and the d3 library for presenting the graph of the historical data for the. The bulk of the client-side application code can be found in the file **rates.html**



	danc.fxutils = ( function() {

		// …

		function loadRates() {
			// code for loading currency codes
			// …
		}

 		function loadHistory(ccy) {
			// code for loading 90 day history for the requested currency
			// …
		}

		function renderGraph() {
			// code for rendering the results using d3.js
			// …
		}

	}() );

The graph presented shows a simple line graph of the 90 day history for the selected curreny, using tooltips when hovering over specific points in time.
