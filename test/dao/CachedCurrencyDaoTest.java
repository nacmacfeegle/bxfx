package dao;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.astyanax.model.Column;
import com.netflix.astyanax.model.ColumnList;

public class CachedCurrencyDaoTest {
    private static Logger LOG = LoggerFactory.getLogger(CachedCurrencyDaoTest.class);
    
    private static final String CACHE_ID_DAILY = "ccydaily";

    @Test
    public void testRead1() throws Exception {
        CachedCurrencyDao dao = new CachedCurrencyDao("localhost:9160", "bxfxdb");
        log(dao.read(CACHE_ID_DAILY));
    }

    @Test
    public void testWrite() throws Exception {
        CachedCurrencyDao dao = new CachedCurrencyDao("localhost:9160", "bxfxdb");
        Map<String, String> ccy = new HashMap<String, String>();
        ccy.put("lastupdate", new Long(System.currentTimeMillis()).toString());
        ccy.put("xmldata", "<xml><node>todo</node></xml>");
        try {
        	dao.write(CACHE_ID_DAILY,ccy);
        } catch (Exception e)  {
        	fail();
        }
    }

    @Test
    public void testRead2() throws Exception {
        CachedCurrencyDao dao = new CachedCurrencyDao("localhost:9160", "bxfxdb");
        log(dao.read(CACHE_ID_DAILY));
    }

    @Test
    public void testDelete() throws Exception {
        CachedCurrencyDao dao = new CachedCurrencyDao("localhost:9160", "bxfxdb");
        dao.delete(CACHE_ID_DAILY);
    }
    

    public void log(ColumnList<String> columns) {
        for (Column<String> column : columns) {
            LOG.debug("[" + column.getName() + "]->[" + column.getStringValue() + "]");
        }
    } 
}
