import org.junit.*;
import play.test.*;
import play.mvc.*;
import play.mvc.Http.*;

public class ApplicationTest extends FunctionalTest {

    @Test
    public void testThatIndexPageWorks() {
        Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }
    
    @Test
    public void testCurrencyList() {
        Response response = GET("/codes");
        assertIsOk(response);
    }
    
    @Test
    public void testCurrencyHistory() {
        Response response = GET("/rates90/USD");
        assertIsOk(response);
    }
    
}