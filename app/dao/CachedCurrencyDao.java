package dao;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Map;

import models.CachedCurrency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.serializers.StringSerializer;

/**
 * DAO provider for currency caching functionality 
 * @author danc
 */
public class CachedCurrencyDao extends AstyanaxDao {

	private static final Logger LOG = LoggerFactory.getLogger(CachedCurrencyDao.class);
	public static final String TABLE_NAME = "ccycachestr";
	
	private static final ColumnFamily<String, String> COLUMN_FAMILY = new ColumnFamily<String, String>(
			TABLE_NAME, StringSerializer.get(), StringSerializer.get());

	public CachedCurrencyDao(String host, String keyspace) {
		super(host, keyspace);
	}

	/**
	 * Write a row
	 * 
	 * @param rowKey				row key
	 * @param columns				map containing columns to write
	 * 
	 * @throws ConnectionException
	 */
	public void write(String rowKey, Map<String, String> columns) throws ConnectionException {
		MutationBatch mutation = this.getKeyspace().prepareMutationBatch();
		for (Map.Entry<String, String> entry : columns.entrySet()) {
			mutation.withRow(COLUMN_FAMILY, rowKey).putColumn(entry.getKey(),
					entry.getValue(), null);
		}
		mutation.execute();
		LOG.debug("Wrote entry [" + rowKey + "]");
	}

	/**
	 * Read a row
	 * 
	 * @param rowKey	row key
	 * @return			list of columns for specified row
	 * 
	 * @throws ConnectionException
	 */
	public ColumnList<String> read(String rowKey) throws ConnectionException {
		OperationResult<ColumnList<String>> result = this.getKeyspace()
				.prepareQuery(COLUMN_FAMILY).getKey(rowKey).execute();
		ColumnList<String> entry = result.getResult();
		LOG.debug("Read entry [" + rowKey + "]");
		return entry;
	}

	/**
	 * Gets the cached XML string by type
	 * 
	 * @param type					row key for the corresponding type
	 * @return						CachedCurrency item for the type or null if not found
	 * 
	 * @throws ConnectionException
	 */
	public CachedCurrency getCachedCurrencyByType(String type) throws ConnectionException {
		ColumnList<String> col = read(type);

		byte[] xmlData = col.getStringValue("xmldata", "").getBytes();

		long lastUpdated = Long.parseLong(col
				.getStringValue("lastupdated", "0"));

		// return null if defaults assumed
		if (lastUpdated == 0) {
			return null;
		}

		Timestamp ts = new Timestamp(lastUpdated);

		CachedCurrency cc = new CachedCurrency(type, ts, xmlData);
		return cc;
	}

	/**
	 * Perform asynchronous cache refresh
	 * 
	 * @param cc					CachedCurrency item to be cached
	 * 
	 * @throws ConnectionException
	 */
	public void refreshCache(CachedCurrency cc) throws ConnectionException {

		MutationBatch mutation = this.getKeyspace().prepareMutationBatch();
		mutation.withRow(COLUMN_FAMILY, cc.getType()).putColumn("lastupdated",
				new Long(System.currentTimeMillis()).toString(), null);
		mutation.withRow(COLUMN_FAMILY, cc.getType()).putColumn("xmldata",
				new String(cc.getXmlData()), null);

		mutation.executeAsync();
	}

	/**
	 * Remove a row
	 * 
	 * @param rowKey				row key
	 * 
	 * @throws ConnectionException
	 */
	public void delete(String rowKey) throws ConnectionException {
		MutationBatch mutation = this.getKeyspace().prepareMutationBatch();
		mutation.withRow(COLUMN_FAMILY, rowKey).delete();
		mutation.execute();
		LOG.debug("Deleted entry [" + rowKey + "]");

	}
}
