package controllers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jobs.CachingDocumentFetcher;
import models.CurrencyHistory;
import models.RateAtDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import play.Play;
import play.libs.F;
import play.libs.WS;
import play.libs.XPath;
import play.mvc.Controller;

import com.google.gson.Gson;

/**
 * Controller class for the engineering challenge application. Contains methods for rendering the main browser pages
 * as well as methods for responding to AJAX queries. 
 *
 * @author danc
 */
public class Application extends Controller {

	public static HashMap<String, String> DOC_TYPE_MAP = new HashMap<String, String>();
	static {
		DOC_TYPE_MAP.put("ccylist", Play.configuration.getProperty("url.fx.90d"));
		DOC_TYPE_MAP.put("ccydaily", Play.configuration.getProperty("url.fx.daily"));
	}
	
	private static int cacheExpiryPeriod = Integer.parseInt(Play.configuration.getProperty("cache.expiry.period", "3"));
	
	/**
	 * Render the index page
	 */
	public static void index() {
		render();
	}

	/**
	 * Render the main application page
	 */
	public static void rates() {
		render();
	}

	/**
	 * Retrieve the 90 day history for a given currency and send back to the client as JSON
	 * @param ccy currency sought
	 */
	public static void ratesByCcy(String ccy) {
	
		// delegate to doc loader
		Document xmlDoc = getResultXml("ccylist");
	
		List<RateAtDate> rateList = new LinkedList<RateAtDate>();
	
		// use XPath to process the xml and pull out the data for the requested code
		for (Node dayNode : XPath.selectNodes("//Cube/Cube[count(ancestor::*)=2]", xmlDoc.getDocumentElement()) ) {
			String timestamp = (String) dayNode.getAttributes().getNamedItem("time").getTextContent();
			String rateXP = "Cube[@currency='" +ccy +"']";
			Node rateNode = XPath.selectNode(rateXP, dayNode);
			String rateVal = (String) rateNode.getAttributes().getNamedItem("rate").getTextContent();
			rateList.add(new RateAtDate(timestamp, rateVal));
		}

		// wrap result in convenience container for GSON
		CurrencyHistory hist = new CurrencyHistory(ccy, rateList);
		Gson gson = new Gson();
		
		// return to client as JSON
		renderJSON(gson.toJson(hist));
	}

	/**
	 * Get the available currency codes (used for currency selector drop-down)
	 */
	public static void currencyCodes() {
		
		// delegate to doc loader
		Document xmlDoc = getResultXml("ccydaily");
	
		LinkedList<String> ccyList = new LinkedList<String>();
	
		// use XPath to process xml and pull out the curreny codes
		for (Node cCode : XPath.selectNodes("//Cube/Cube/Cube", xmlDoc.getDocumentElement()) ) {
			String ccy = (String) cCode.getAttributes().getNamedItem("currency").getTextContent();
			ccyList.add(ccy);
		}
	
		// wrap result in map for GSON and return to client as JSON
		Map<String, List<String>> codes = new HashMap<String, List<String>>();
		codes.put("codes", ccyList);
		
		Gson gson = new Gson();
		renderJSON(gson.toJson(codes));
	}

	/**
	 * Non-blocking implementation of the currency XML data. 
	 * 
	 * @param type			type of document to fetch (URLs stored in map)
	 * @return				DOM document representation of the currency data
	 */
	public static Document getResultXml(String type) {
		String url = DOC_TYPE_MAP.get(type);
		F.Promise<WS.HttpResponse> r1 = WS.url(url).getAsync();
		WS.HttpResponse httpResponse = await(r1);
		Document xmlDoc = httpResponse.getXml();

		return xmlDoc;
	}
	

	/**
	 * Non-blocking, cache-backed retrieval of the requested data
	 * 
	 * @param type			type of document to fetch (URLs stored in map)
	 * @return				DOM document representation of the currency data
	 */
	public static Document getResultXmlCaching(String type) {
		F.Promise<Document> d = new CachingDocumentFetcher(type, cacheExpiryPeriod).now();
		Document xmlDoc = await(d);
		return xmlDoc;
	}
	
}
