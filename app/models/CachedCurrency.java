package models;

import java.sql.Timestamp;

/**
 * CachedCurrency POJO. 
 * @author danc
 */
public class CachedCurrency {
	
	private String type;
	private Timestamp lastUpdated;
	private byte[] xmlData;
	
	public CachedCurrency(String type, Timestamp lastUpdated, byte[] xmlData) {
		super();
		this.type = type;
		this.lastUpdated = lastUpdated;
		this.xmlData = xmlData;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Timestamp getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public byte[] getXmlData() {
		return xmlData;
	}
	public void setXmlData(byte[] xmlData) {
		this.xmlData = xmlData;
	}
	
}
