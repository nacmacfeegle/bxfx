package models;

/**
 * POJO representation for a rate at a date
 * @author danc
 */
public class RateAtDate {
	
	private String date;
	private String rate;

	public RateAtDate(String date, String rate) {
		this.date = date;
		this.rate = rate;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getRate() {
		return rate;
	}
}
