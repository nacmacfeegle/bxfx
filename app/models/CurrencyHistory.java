package models;

import java.util.List;

/**
 * CurrencyHistory POJO 
 * @author danc
 */
public class CurrencyHistory {
	private String currency;
	private List<RateAtDate> history;
	
	public CurrencyHistory(String ccy, List<RateAtDate> history) {
		this.currency = ccy;
		this.history = history;
	}
	
	public String getCurrency() {
		return this.currency;
	}
	
	public List<RateAtDate> getHistory() {
		return this.history;
	}

}
