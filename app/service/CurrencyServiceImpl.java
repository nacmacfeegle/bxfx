package service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;

import models.CachedCurrency;
import dao.CachedCurrencyDao;

/**
 * Implementation of the CurrencyService using an astyanax dao
 * @author danc
 */
public class CurrencyServiceImpl implements CurrencyService {

    private static Logger LOG = LoggerFactory.getLogger(CurrencyServiceImpl.class);

    private CachedCurrencyDao dao;
	
	public CurrencyServiceImpl() {
		dao = new CachedCurrencyDao("localhost", "bxfxdb"); 
	}

	/*
	 * (non-Javadoc)
	 * @see service.CurrencyService#getCachedCurrency(java.lang.String)
	 */
	public CachedCurrency getCachedCurrency(String type) {
		
		CachedCurrency cc = null;
		try {
			return dao.getCachedCurrencyByType(type);
		} catch (ConnectionException e) {
			// going to return null here
		}
		return cc;
	}

	/*
	 * (non-Javadoc)
	 * @see service.CurrencyService#refreshCache(models.CachedCurrency)
	 */
	public void refreshCache(CachedCurrency cc) {
		try {
			dao.refreshCache(cc);
		} catch (ConnectionException e) {
			LOG.error("Problem refreshing cached currency", e);
		}
	}

}
