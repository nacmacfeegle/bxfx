package service;

import models.CachedCurrency;

/**
 * Currency service interface. Used to retrieve and refresh cached items
 * @author danc
 */
public interface CurrencyService {

	/**
	 * Retrieve a CacchedCurrency object by type 
	 * @param type
	 * @return
	 */
	CachedCurrency getCachedCurrency(String type);

	/**
	 * Refresh the cache with the specified CachedCurrency object
	 * @param cc
	 */
	void refreshCache(CachedCurrency cc);
	
}
