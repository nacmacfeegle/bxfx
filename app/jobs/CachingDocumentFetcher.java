package jobs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import models.CachedCurrency;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import play.jobs.Job;
import play.libs.WS;
import service.CurrencyService;
import service.CurrencyServiceImpl;
import controllers.Application;

/**
 * Play job to fetch an XML document, backed by a Cassandra cache. If the document is not found, it is loaded from the
 * external API and added to the cache. Subsequent reads will use the cached data.
 * 
 * @author danc
 *
 */
public class CachingDocumentFetcher extends Job<Document> {

	private static Logger LOG = LoggerFactory.getLogger(CachingDocumentFetcher.class);
		
	/**
	 * Currency service
	 */
	private CurrencyService ccySvc;

	/**
	 * Type of document to fetch
	 */
	private String type;

	/**
	 * Number of hours that a cached item stays "fresh"
	 */
	private int expiryHours;

	public CachingDocumentFetcher(String type, int hours) {
		this.type = type;
		this.expiryHours = hours;
		ccySvc = new CurrencyServiceImpl();
	}


	public Document doJobWithResult() {
		return fetchXml();
	}

	/**
	 * Fetches the required document from the cache. Re-fetches documents if timestamp is after configured expiry
	 * @return
	 */
	private Document fetchXml() {

		CachedCurrency cc = ccySvc.getCachedCurrency(type);

		// might not have been saved before
		if (cc != null) {

			if (!hasExpired(cc.getLastUpdated(), expiryHours)) {
				return buildXml(cc.getXmlData());
			} else {
				byte[] xmlBytes = loadXml(type);
				cc.setXmlData(xmlBytes);
				ccySvc.refreshCache(cc);
			}
		} else {
			// create cache entry
			byte[] xmlBytes = loadXml(type);
			cc = new CachedCurrency(type, null, xmlBytes);
			ccySvc.refreshCache(cc);
		}

		return buildXml(cc.getXmlData());
	}

	/**
	 * Create a DOM document from a byte array
	 * 
	 * @param xmlData			byte array
	 * @return					deserialized document
	 */
	private Document buildXml(byte[] xmlData) {
		Document doc = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(false);
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.parse(new ByteArrayInputStream(xmlData));
		} catch (Exception e) {
			LOG.error("Unable to build xml from data bytes");
		}
		return doc;
	}

	/**
	 * Loads a document by type
	 * 
	 * @param type		document type to load
	 * @return 			serialized document as a byte array
	 */
	private byte[] loadXml(String type) {
		
		byte[] data = null;
		String url = Application.DOC_TYPE_MAP.get(type);
		WS.HttpResponse httpResponse = WS.url(url).get();
		Document doc = httpResponse.getXml();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(bos);
			transformer.transform(source, result);
		} catch (Exception e) {
			LOG.error("Unable to get data bytes from doc", e);
		}
		
		data = bos.toByteArray();
		return data;
	}

	/**
	 * Expiry check: is the content still fresh?
	 * 
	 * @param ts		timestamp
	 * @param hours		expiry period (in hours)
	 * @return			true if expired, false otherwise
	 */
	private boolean hasExpired(Timestamp ts, int hours) {

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(ts.getTime());
		cal.add(Calendar.HOUR, hours);
		
		Timestamp expiryDate = new Timestamp(cal.getTimeInMillis());
		Timestamp now = new Timestamp(System.currentTimeMillis());

		return (now.after(expiryDate));
	}
}
